import { ConsoleLogger } from '@nestjs/common';
import logger from 'pino';

export class PinoLogger extends ConsoleLogger {
  private readonly console;

  constructor() {
    super();
    this.console = logger();
  }

  log(message: string, ...data: any[]) {
    this.console.info(message, data);
  }
}

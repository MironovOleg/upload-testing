export enum EType {
  'file.upload' = 'file.upload',
  'file.cropped' = 'file.cropped',
}

export type TEventData = {
  file?: Express.Multer.File;
};

export default class Event {
  private readonly type: EType;
  private readonly data?: TEventData;

  constructor(type: EType, data: TEventData = null) {
    this.type = type;
    this.data = data;
  }

  public getType(): EType {
    return this.type;
  }

  public getData(): TEventData | null {
    return this.data;
  }
}

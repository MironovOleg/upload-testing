import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { UploadService } from './services/upload.service';
import { ApiBody, ApiConsumes } from '@nestjs/swagger';
import { diskStorage } from 'multer';
import { v4 as uuid } from 'uuid';
import { extname } from 'path';

@Controller()
export class AppController {
  constructor(private readonly uploadService: UploadService) {}

  @Post('/upload')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      required: ['file'],
      properties: {
        file: {
          type: 'file',
        },
      },
    },
  })
  @UseInterceptors(
    FileInterceptor('file', {
      limits: {
        fileSize: +process.env.MAX_FILE_SIZE,
      },
      fileFilter: (req, file, cb) => {
        if (
          process.env.IMAGE_MIME_TYPES.split(',').indexOf(file.mimetype) === -1
        ) {
          cb(
            new HttpException(
              `Unsupported file "${extname(file.originalname)}"`,
              HttpStatus.BAD_REQUEST,
            ),
            false,
          );
        } else {
          cb(null, true);
        }
      },
      storage: diskStorage({
        destination: './tmp',
        filename: (req, file, cb) => {
          req.body.uuid = uuid();
          cb(null, `${req.body.uuid}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  uploadImage(
    @UploadedFile() file: Express.Multer.File,
    @Body('uuid') uuid: string,
  ): string {
    return this.uploadService.save(file, uuid);
  }
}

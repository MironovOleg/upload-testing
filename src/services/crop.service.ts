import { Injectable } from '@nestjs/common';
import { EventEmitter2, OnEvent } from '@nestjs/event-emitter';
import sharp from 'sharp';
import * as fs from 'fs';
import Event, { EType } from '../utils/Event';

@Injectable()
export class CropService {
  constructor(private readonly eventEmitter: EventEmitter2) {}

  @OnEvent(EType['file.upload'], {
    async: true,
  })
  async handleFileUploadEvent(event: Event): Promise<boolean> {
    const sizes: Array<number[]> = JSON.parse(process.env.CROP_SIZES);
    const file = event.getData().file;

    return new Promise((resolve) =>
      Promise.all(
        sizes.map((size) =>
          sharp(file.path)
            .resize(size[0], size[1])
            .toFile(`crop/${size[0]}x${size[1]}_${file.filename}`),
        ),
      )
        .then(() => {
          fs.unlinkSync(file.path);
          this.eventEmitter.emit(
            EType['file.cropped'],
            new Event(EType['file.cropped']),
          );
          resolve(true);
        })
        .catch(() => {
          resolve(false);
        }),
    );
  }
}

import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import Event, { EType } from '../utils/Event';

@Injectable()
export class UploadService {
  constructor(private readonly eventEmitter: EventEmitter2) {}

  save(file: Express.Multer.File, uuid: string): string {
    this.eventEmitter.emit(
      EType['file.upload'],
      new Event(EType['file.upload'], { file }),
    );
    return uuid;
  }
}

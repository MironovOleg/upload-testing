import { Injectable, Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import Event from '../utils/Event';

@Injectable()
export class LogService {
  private readonly logger = new Logger(LogService.name);

  @OnEvent('**')
  listenEvents(event: Event): void {
    this.logger.log(event.getType());
  }
}

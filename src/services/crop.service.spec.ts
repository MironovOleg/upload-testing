import { CropService } from './crop.service';
import { EventEmitter2 } from 'eventemitter2';
import Event, { EType } from '../utils/Event';
import * as fs from 'fs';

describe('CatsController', () => {
  let cropService: CropService;
  let eventEmitter: EventEmitter2;
  let file: Express.Multer.File;

  beforeEach(() => {
    fs.copyFileSync('test/boy.png', 'test/mock_boy.png');
    process.env.CROP_SIZES = '[[100, 100]]';
    eventEmitter = new EventEmitter2();
    cropService = new CropService(eventEmitter);
    file = {
      path: 'test/mock_boy.png',
      originalname: 'mock_boy.png',
      filename: 'mock_boy.png',
      fieldname: 'mock_boy.png',
      mimetype: 'image/png',
      encoding: 'buffer',
      size: 1000,
      stream: undefined,
      destination: undefined,
      buffer: fs.readFileSync('test/mock_boy.png'),
    };
  });

  describe('handleFileUploadEvent', () => {
    it('success: check create crop images', async () => {
      const event = new Event(EType['file.upload'], {
        file,
      });
      expect(await cropService.handleFileUploadEvent(event)).toBe(true);
    });

    it('error: check create crop images', async () => {
      file.path = 'mock_path';
      const event = new Event(EType['file.upload'], {
        file,
      });
      expect(await cropService.handleFileUploadEvent(event)).toBe(false);
    });
  });
});

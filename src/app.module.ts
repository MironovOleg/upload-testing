import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { UploadService } from './services/upload.service';
import { ConfigModule } from '@nestjs/config';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { CropService } from './services/crop.service';
import { LogService } from './services/log.service';

@Module({
  imports: [
    ConfigModule.forRoot(),
    EventEmitterModule.forRoot({
      wildcard: true,
    }),
  ],
  controllers: [AppController],
  providers: [UploadService, CropService, LogService],
})
export class AppModule {}
